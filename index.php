<?php 
	require_once 'Persona.php';
	require_once 'Empleado.php';
	require_once 'Empresa.php';
	require_once 'Telefono.php';

	
	$empresa = new Empresa('Empresa Generica','Eusebio Ayala 123');

	$e1 = new Empleado('Juan','Pérez','111111','Japón 123','30','111111',$empresa);
	$t1 = new Telefono('0961-111111','ejecutivo',$e1);
	$t2 = new Telefono('0971-111111','personal',$e1);
	$e1->setTelefono($t1);
	$e1->setTelefono($t2);
	$e2 = new Empleado('Maria','González','222222','Estados Unidos 456','27','222222',$empresa);
	$t3 = new Telefono('0981-111111','ejecutivo',$e2);
	$e2->setTelefono($t3);
	$e3 = new Empleado('José','Rodriguez','333333','Brasil 789','40','333333',$empresa);
	$t4 = new Telefono('0991-111111','ejecutivo',$e3);
	$e3->setTelefono($t4);

	$empresa->setEmpleado($e1);
	$empresa->setEmpleado($e2);
	$empresa->setEmpleado($e3);
	$empresa->getEmpleados();
 ?>