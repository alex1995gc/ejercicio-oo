<?php 
	class Persona {
		protected string $nombre,$apellido,$cedula,$direccion; 
		protected int $edad;
		protected $telefonos;

		public function __construct($n,$a,$c,$d,$e){
	        $this->nombre = $n;
	        $this->apellido = $a;
	        $this->cedula = $c;
	        $this->direccion = $d;
	        $this->edad = $e;
	        $this->telefonos = array();
    	}

		public function __get($name){
	        if (property_exists($this, $name)){
	            return $this->$name;
	        }else
	            throw new Exception("No existe la propiedad");
	    }

    	public function __set($name,$value){
	        if (property_exists($this, $name)){
	            return $this->$name = $value;
	        }else
	            throw new Exception("No existe la propiedad");
	    }

		public function getTelefonos()
	    {
	        foreach ($this->telefonos as $index=>$value)
	            echo 'Teléfono: '.$value->numero.' '.$value->tipo.'<br/>';
	    }
		public function setTelefono(Telefono $t){
        	$this->telefonos[] = $t;
    	}
	}
 ?>