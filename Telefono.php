<?php 
	class Telefono {
	    private string $tipo,$numero;
	    private $persona;

	    public function __construct(string $t, string $n,Empleado $e){
	        $this->tipo = $t;
	        $this->numero = $n;
	        $this->persona = $e;
	    }

	    public function __get($name){
	        if (property_exists($this, $name)){
	            return $this->$name;
	        }else
	            throw new Exception("No existe la propiedad");
	    }

	    public function __set($name,$value){
	        if (property_exists($this, $name)){
	            return $this->$name = $value;
	        }else
	            throw new Exception("No existe la propiedad");
	    }
	}
 ?>