<?php 
	class Empleado extends Persona {
	    private int $codigoEmpleado;
	    private $empresa;

	    public function __construct($n, $a, $c, $d, $e, $ce,Empresa $emp)
	    {
	        $this->codigoEmpleado = $ce;
	        $this->empresa = $emp;
	        parent::__construct($n, $a, $c, $d, $e);
	    }

	    public function __get($name){
	        if (property_exists($this, $name)){
	            return $this->$name;
	        }else
	            throw new Exception("No existe la propiedad");

	    }

	    public function __set($name,$value){
	        if (property_exists($this, $name)){
	            return $this->$name = $value;
	        }else
	            throw new Exception("No existe la propiedad");

	    }

	    public function imprimirEmpleado()
	    {
	        
	        echo 'Nombre: '.$this->nombre.'/ Apellido: '.$this->apellido.'/ Cédula: '.$this->cedula.'/ Dirección: '.$this->direccion.
	            '/ Edad: '.$this->edad.'/ Código: '.$this->codigoEmpleado.'<br/>';

	        echo $this->getTelefonos();
	        echo '<br/>';
	    }
}
 ?>