<?php 
	class Empresa {
        protected string $nombre;
        protected string $direccion;
        protected $empleados;

        public function __construct(string $n, string $d){
            $this->nombre = $n;
            $this->direccion = $d;
            $this->empleados = array();
        }

        public function __get($name){
            if (property_exists($this, $name)){
                return $this->$name;
            }else
                throw new Exception("No existe la propiedad");
        }

        public function __set($name,$value){
            if (property_exists($this, $name)){
                return $this->$name = $value;
            }else
                throw new Exception("No existe la propiedad");
        }

        public function getEmpleados()
        {
            echo "Empleados de la empresa $this->nombre:<br/><br/>";
            foreach ( $this->empleados as $index => $value )
                echo $value->imprimirEmpleado();
        }
        public function setEmpleado(Empleado $e){
            $this->empleados[] = $e;
        }
    }
 ?>